package org.itstep.qa.homework.train.entity;

public class Station {
    private String name;
    private int x;

    public Station (String name, int x){
        this.name = name;
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public String getName(){
      return name;
    }
}
