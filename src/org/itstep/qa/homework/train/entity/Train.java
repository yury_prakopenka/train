package org.itstep.qa.homework.train.entity;

public class Train {

    private int xCurrent;
    private int speed;
    private String name;
    private String[] typesOfTrain = {"Civil", "Work"};
    private String typeOfTrain;
    private int nextPos;
    private int destination;
    private int stepOfTrain;

    public Train(String name, int numberOfType, int speed, int destination, int start) {
        this.name = name;
        this.typeOfTrain = typesOfTrain[numberOfType];
        this.speed = speed;
        this.destination = destination;
        this.xCurrent = start;
    }

    public int getxCurrent() {
        return xCurrent;
    }

    public int getSpeed() {
        return speed;
    }

    public String getName() {
        return name;
    }

    public String getTypeOfTrain() {
        return typeOfTrain;
    }

    public int getStepOfTrain() {
        return stepOfTrain;
    }

    public void move (boolean nextTrain) {
        if (nextTrain) {xCurrent += speed; stepOfTrain += 1;}
        if (isArrived()) xCurrent = destination;
    }

    public boolean findNextTrain (int i, Train[] trains) {
        boolean nextTrain = true;
        nextPos = xCurrent + speed;

        if(xCurrent < 50 || xCurrent > 65) {
            for (int j = i; j >= 0; j--) {
                if(trains[j].isArrived()) continue;
                if (nextPos >= trains[j].xCurrent && i != j) nextTrain = false;
            }
            return nextTrain; } else {

            for (int j = i; j >= 0; j--) {
                if(trains[j].isArrived()) continue;
                if (nextPos == trains[j].xCurrent && i != j) nextTrain = false;
                else if (nextPos > trains[j].xCurrent &&
                        (typeOfTrain.equals("Work") || (typeOfTrain.equals(trains[j].typeOfTrain))) &&
                         i != j && trains[j].xCurrent != 0) nextTrain = false;
            }
            return nextTrain; }
    }

    public boolean isArrived() {
        if (xCurrent >= destination) return true;
        else return false;
    }


    @Override
    public String toString() {
        return "Train{" +
                " name='" + name + '\'' +
                ", typeOfTrain='" + typeOfTrain + '\'' +
                ", speed=" + speed +
                ", x=" + xCurrent +
                ", destination=" + destination +
                '}';
    }
}
