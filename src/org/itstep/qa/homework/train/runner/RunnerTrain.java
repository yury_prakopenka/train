package org.itstep.qa.homework.train.runner;

import org.itstep.qa.homework.train.entity.Station;
import org.itstep.qa.homework.train.entity.Train;

import java.util.Arrays;
import java.util.Random;

public class RunnerTrain {
    public static void main(String[] args) {

        Station[] stations = {
                new Station("Gadyukino", 0),
                new Station("Vedmino", 100),
                new Station("Chupakabrino", 180)
        };

        Train[] trains = initTrain(13, stations);

        System.out.println(Arrays.toString(trains));
        int timeStep = 1, trainsOnLine;
        int arrivedTrains = 0;

        while (arrivedTrains != trains.length) {

            if (timeStep < trains.length) trainsOnLine = timeStep;
            else trainsOnLine = trains.length;

            for (int i = 0; i < trainsOnLine; i++) {
                if (trains[i].isArrived()) continue;
                trains[i].move(trains[i].findNextTrain(i, trains));
                if (trains[i].isArrived()) {
                    arrivedTrains++;
                    System.out.println(trains[i].getName() + ", type: " + trains[i].getTypeOfTrain() + ", speed: " +
                            trains[i].getSpeed() + ". Arrived to " + findStation(stations, trains[i].getxCurrent()) + " by "
                            + trains[i].getStepOfTrain() + " steps. It's current coordinate is " + trains[i].getxCurrent());
                } else
                    System.out.println(trains[i].getName() + " " + trains[i].getxCurrent());
                trains = controlTurn(trains);
            }

            timeStep++;
            System.out.println("------------------------");
        }
        System.out.println(Arrays.toString(trains));
    }


    private static Train[] initTrain(int count, Station[] station) {
        Random random = new Random();
        Train[] trains = new Train[count];
        for (int i = 0; i < trains.length; i++) {
            trains[i] = new Train("Train" + (i + 1),
                    random.nextInt(2),
                    random.nextInt(10) + 1,
                    station[random.nextInt(2) + 1].getX(),
                    station[0].getX()
            );
        }
        return trains;
    }

    private static Train[] controlTurn(Train[] trains) {
        Train tempTrain;

        for (int i = 1; i < trains.length; i++) {
            for (int j = trains.length - 1; j >= i; j--) {
                if (trains[j - 1].getxCurrent() < trains[j].getxCurrent()) {
                    tempTrain = trains[j - 1];
                    trains[j - 1] = trains[j];
                    trains[j] = tempTrain;
                }
            }
        }
        return trains;
    }

    private static String findStation(Station[] station, int x) {
        for (int i = 0; i < station.length; i++) {
            if (x == station[i].getX()) return station[i].getName();
        }
        return null;
    }
}

